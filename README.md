# mycOG
## Scalable phylogenetic orthology inference (OrthoFinder + Mycotools)

### DISCLAIMER:
`mycOG` is currently in a beta state and while I appreciate bringing issues to my attention,
I will prioritize my own research and cannot guarantee I will resolve these issues in a timely manner. 
My hope is that everything will be stable upon publication (2022) and that any issues will be resolved.

![OrthoFinder workflow](assets/Workflow.png)
*Figure 1: Automatic OrthoFinder analysis*

## What does OrthoFinder do?
OrthoFinder is a fast, accurate and comprehensive platform for comparative genomics. It finds **orthogroups** and **orthologs**, infers **rooted gene trees** for all orthogroups and identifies all of the **gene duplication events** in those gene trees. It also infers a **rooted species tree** for the species being analysed and maps the gene duplication events from the gene trees to branches in the species tree. OrthoFinder also provides **comprehensive statistics** for comparative genomic analyses. OrthoFinder is simple to use and all you need to run it is a set of protein sequence files (one per species) in FASTA format.

For more details see the OrthoFinder papers below or visit OrthoFinder's [git repository](https://github.com/davidemms/orthofinder).

[Emms, D.M. and Kelly, S. **(2019)** _OrthoFinder: phylogenetic orthology inference for comparative genomics._ **Genome Biology** 20:238](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1832-y)

[Emms, D.M. and Kelly, S. **(2015)** _OrthoFinder: solving fundamental biases in whole genome comparisons dramatically improves orthogroup inference accuracy._ **Genome Biology** 16:157](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-015-0721-2)

### Options for starting an analysis
**-d** \<.db\>: Start analysis from MycotoolsDB 
**-b** \<dir\>: Start analysis from BLAST results
**-fm** \<.txt\>: Start analysis from MCL results
**-fg** \<dir\>: Start analysis from orthogroups results
**-ft** \<dir\>: Start analysis from gene trees results
 
### Options for stopping an analysis 
**-op**: Stop after preparing input files for all-vs-all sequence search (e.g. BLAST/DIAMOND)  
**-og**: Stop after inferring orthogroups  
**-os**: Stop after writing sequence files for orthogroups (requires '-M msa')  
**-oa**: Stop after inferring mulitple sequence alignments for orthogroups (requires '-M msa')  
**-ot**: Stop after inferring gene trees for orthogroups  

### Options controlling the workflow
**-B** BigMCL approach - Isolate disconnected subgraphs and cluster upon those    
**-M** \<opt\>: Use MSA or DendroBLAST gene tree inference, opt=msa,dendroblast [default=dendroblast]    

### Options controlling the programs used
**-S** \<opt\>: Sequence search program opt=blast,diamond,mmseqs,... user-extendable [Default = diamond]   
**-A** \<opt\>: MSA program opt=mafft,muscle,... user-extendable (requires '-M msa') [Default = mafft]   
**-T** \<opt\>: Tree inference program opt=fasttree,raxml,iqtree,... user-extendable (requires '-M msa') [Default = fasttree]    
 
 ### Further options
**-d**: Input is DNA sequences
**-t** \<int\>: Number of threads for sequence search, MSA & tree inference [Default is number of cores on machine]  
**-a** \<int\>: Number of parallel analysis threads for internal, RAM intensive tasks [Default = 1]  
**-s** \<file\>: User-specified rooted species tree  
**-I** \<int\>: MCL inflation parameter [Default = 1.5]  
**-x** \<file\>: Info for outputting results in OrthoXML format  
**-p** \<dir\>:  Write the temporary pickle files to \<dir\>  
**-1**: Only perform one-way sequence search  
**-X**: Don't add species names to sequence IDs in output files  
**-y**: Split paralogous clades below root of a HOG into separate HOGs  
**-z**: Don't trim MSAs (columns>=90% gap, min. alignment length 500)  
**-n** \<txt\>: Name to append to the results directory  
**-o** \<txt\>: Non-default results directory  
**-h**: Print this help text  
